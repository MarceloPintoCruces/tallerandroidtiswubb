package com.example.marcelo.ejemploclienteresttisw;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marcelo on 21-06-16.
 *
 */
public class RESTHandler {

    private RequestQueue queue;
    private StringRequest stringRequest;
    private JsonObjectRequest jsonRequest;
    private String uri;
    private Context context;

    //Interfaces usadas para manejar los callbacks de cada petición
    private VolleyCallback callback;
    private VolleyCallbackJsonObject callbackjsonobject;

    private static final String TAG = "RESTHandler";


    public RESTHandler(String uri, Context context){

        this.uri = uri;
        this.context = context;
        queue = Volley.newRequestQueue(this.context);
    }



    public void actionGETArray(VolleyCallback mcallback){

        this.callback = mcallback;


        stringRequest = new StringRequest(Request.Method.GET, this.uri, //Petición GET, a la url http://tisw.marcelopintocruces.com
                new Response.Listener<String>() { //Un hilo que espera el proceso de enviar la petición y esperar la respuesta
                    @Override
                    public void onResponse(String response) { //Este callback se invoca una vez que el servidor responde

                        Log.i(TAG, "La respuesta del server es: "+ response);

                        try {
                            JSONObject respuesta = new JSONObject(response);
                            callback.onSuccess(respuesta.getJSONArray("contactos"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() { // además, al string request se le añade un parámetro de un listener de errores de red
            @Override
            public void onErrorResponse(VolleyError error) { //si hay un error en la petición, se puede manejar, tal como una exception
                Log.e(TAG, "Error al enviar petición");
            }
        });

        queue.add(stringRequest);


    }

    public void actionPOST(VolleyCallbackJsonObject mcallback, JSONObject jObj){
        //Ojo que acá no es un stringRequest, si no un JsonObjectRequest, significa que la respuesta se maneja como un objeto JSON
        this.callbackjsonobject = mcallback;

        jsonRequest = new JsonObjectRequest(Request.Method.POST, this.uri, jObj, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                callbackjsonobject.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error al enviar petición");
            }
        });

        queue.add(jsonRequest);
    }

    //Declaraciones de las interfaces para manejar los callbacks

    public interface VolleyCallback{
        void onSuccess(JSONArray result);
    }

    public interface VolleyCallbackJsonObject{
        void onSuccess(JSONObject result);
    }


}
