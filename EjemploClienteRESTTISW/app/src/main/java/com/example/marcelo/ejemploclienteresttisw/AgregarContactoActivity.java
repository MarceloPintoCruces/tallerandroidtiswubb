package com.example.marcelo.ejemploclienteresttisw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AgregarContactoActivity extends AppCompatActivity {

    private static final String TAG = "AgregarContactoActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_contacto);

        Button mAceptar = (Button)findViewById(R.id.btn_guardar);

        mAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //falta validar campos nulos

                final JSONObject contacto = new JSONObject();
                try {

                    //Recuperamos los textos de cada EditText y lo convertimos en un objeto JSON
                    contacto.put("nombre",((EditText)findViewById(R.id.txt_nombre)).getText());
                    contacto.put("apellido",((EditText)findViewById(R.id.txt_apellido)).getText());
                    contacto.put("celular",((EditText)findViewById(R.id.txt_celular)).getText());
                    contacto.put("email",((EditText)findViewById(R.id.txt_email)).getText());

                    //OJO: Acá la Uri apunta a index2.php, ya que en mi host hay una regla apache que cada petición (sea GET, POST,etc) genera una redirección a index usando GET
                    //o sea, NUNCA LLEGARÍA NINGUN OTRO MÉTODO QUE GET. Por eso, cuando se da este caso y no se puede cambiar, conviene no apuntar a index.php si no a otro script
                    RESTHandler rest = new RESTHandler("http://tisw.marcelopintocruces.com/api/contactos/index2.php",getApplicationContext());
                    rest.actionPOST(new RESTHandler.VolleyCallbackJsonObject() { //Este método genera una petición POST a la API REST, recibe como parámetro un callback y un objeto JSON
                        @Override
                        public void onSuccess(JSONObject result) { //La API REST responde con un booleano, si se insertó o no
                            try {
                                if (result.getBoolean("exito")){
                                    Toast.makeText(AgregarContactoActivity.this, "Contacto Agregado!", Toast.LENGTH_SHORT).show();
                                    Log.i(TAG, "Contacto: "+contacto.toString()+" Creado");
                                    finish(); //Acá finalizamos la activity y volvemos "atraz" (por eso la importancia del ciclo de vida de una activity)
                                }else{
                                    Toast.makeText(AgregarContactoActivity.this, "Error al agregar contacto!", Toast.LENGTH_SHORT).show();
                                    Log.e(TAG, "Contacto: "+contacto.toString()+" No Creado, se produjo un error");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(TAG, "Excepcion de JSON");
                            }
                        }
                    }, contacto);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
}
