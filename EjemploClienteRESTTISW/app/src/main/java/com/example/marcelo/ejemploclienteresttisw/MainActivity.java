package com.example.marcelo.ejemploclienteresttisw;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Invocamos al método setListOfContacts, para que se actualice de inmediato la lista de contactos
        setListOfContacts();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Lanzamos una nueva Activity que permite agregar un contacto en nuestro servidor, usando una API REST

                Intent i = new Intent(getApplicationContext(),AgregarContactoActivity.class);
                startActivity(i);

            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        //Cada vez que se regrese a la activity, se actualiza la lista.
        setListOfContacts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setListOfContacts(){

        /**
         * setListOfContacts: Método que genera una petición GET a una API REST que trae todos los contactos del servidor.
         */

        final ArrayList<String> lista_contactos = new ArrayList<String>(); //utilizado para poblar la ListView

        final ListView mContactos = (ListView) findViewById(R.id.lista_contactos); //Instanciamos la ListView

        //Nuestra clase RESTHandler recibe como parámetros la uri de la API y el contexto actual (para tareas de red)
        RESTHandler rest = new RESTHandler("http://tisw.marcelopintocruces.com/api/contactos",getApplicationContext());


        rest.actionGETArray(new RESTHandler.VolleyCallback() { //el método actionGETArray recibe como parámetro un CallBack que maneja la respuesta
            @Override
            public void onSuccess(JSONArray result) { //EL servidor ya respondió
                JSONArray jArray = result;
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jObj = null;
                    try {
                        jObj = jArray.getJSONObject(i); //Obtenemos cada objeto JSON
                        String contacto = jObj.getString("nombre")+" "+jObj.getString("apellido")+" - "+jObj.getString("celular");//Concatenamos formando un solo string para el ListView

                        lista_contactos.add(contacto); //Lo añadimos a la lista de contactos

                        //Para poblar una ListView se utiliza un ArrayAdapter, que recibe como parámetro el contexto, un Layout para la lista y el array con los datos
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                getApplicationContext(),
                                android.R.layout.simple_list_item_1,
                               lista_contactos);

                        mContactos.setAdapter(arrayAdapter); //Seteamos el adapter en la ListView

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
