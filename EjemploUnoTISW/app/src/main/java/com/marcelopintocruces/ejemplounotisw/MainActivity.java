package com.marcelopintocruces.ejemplounotisw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*TextView es el tipo de dato que representa un TextView, algo como un JLabel en java*/
    private TextView txtNombre;
    /*EditText es el tipo de dato que representa un EditText, algo como un JTextField en java*/
    private EditText etNombre;
    /*Button es el tipo de dato que representa un Button, algo como un JButton en java*/
    private Button btnAceptar;

    private final static String TAG = "MainActivity"; //esto es una buena práctica para utilizar los Loggers

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Toast.makeText(getApplicationContext(), "onCreate!", Toast.LENGTH_SHORT).show();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        txtNombre = (TextView) findViewById(R.id.txt_nombre);
        /*el método findViewById recibe como parámetro el ID de un elemento del layout representado por un entero
        * En este caso, R es una clase .java que maneja los id de los layouts, entre otras cosas
        * Al escribir R.id está accediendo a la clase id, y finalmente a su propiedad o atributo txt_nombre que se autogenera al
        * compilar el proyecto*/
        txtNombre.setText("Hola Mundo!");
        /*Así, el objeto txtNombre de la clase TextView puede ser manejado accediendo a sus métodos, en este caso, al método
        * setText que recibe como parámetro un CharSecuence (algo como un String de Java SE) que cambia la etiqueta android:text del XML de layout*/

        etNombre = (EditText) findViewById(R.id.et_nombre);

        btnAceptar = (Button) findViewById(R.id.btn_aceptar);



        /*setOnClickListener añade un listener que escucha el evento OnClick
        * Como parámetro recibe un objeto tipo View, que maneja los eventos de las UI's
        * En este caso, es un método OnClickListener que sobreescribe el método onClick*/
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence et_nombre_result = etNombre.getText(); //Recuperamos el texto del EditText que ingresó el usuario
                txtNombre.setText(et_nombre_result); //Cambiamos el texto del TextView por el texto que ingresó el usuario

                Log.i(TAG, "Click en el botón ACEPTAR"); //Esta línea permite al Logger informar por Android Monitor un mensaje de Información.

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(getApplicationContext(), "onStart!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(getApplicationContext(), "onResume!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(getApplicationContext(), "onPause!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(getApplicationContext(), "onRestart!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(getApplicationContext(), "onStop!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(getApplicationContext(), "onDestroy!", Toast.LENGTH_SHORT).show();
    }
}
