package com.example.marcelo.ejemplowebservicetisw;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class ClienteWS extends AsyncTask<String, Integer, String> {

    private Context context;

    private static final String SOAP_ACTION = "precio#getPrecio";
    private static final String OPERATION_NAME = "getPrecio";
    private static final String WSDL_TARGET_NAMESPACE = "precio";
    public static final String SOAP_ADDRESS = "http://tisw.marcelopintocruces.com/ejemploWebService/servicioserver.php";

    public ClienteWS(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {

        String result = null;

        SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,
                OPERATION_NAME);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER10);

        // Con esta opción indicamos que el web service no es .net
        envelope.dotNet = false;

        envelope.setOutputSoapObject(request);

        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);

        // Enviando un parámetro al web service
        request.addProperty("servicio", params[0]);

        try {

            // Enviando la petición al web service
            httpTransport.call(SOAP_ACTION, envelope);

            // Recibiendo una respuesta del web service
            SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
                    .getResponse();

            result = resultsRequestSOAP.toString();

        } catch (IOException | XmlPullParserException e) {
            Log.e("Error", e.getMessage());
            result = e.getMessage();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        // Mostramos la respuesta del web service
        Toast.makeText(context, result, Toast.LENGTH_LONG).show();
        callBackDeRetorno(result);

    }

    protected String callBackDeRetorno(String valor){
        return valor;
    }

}