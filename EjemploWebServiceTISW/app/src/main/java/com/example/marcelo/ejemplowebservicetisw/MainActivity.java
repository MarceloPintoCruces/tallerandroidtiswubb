package com.example.marcelo.ejemplowebservicetisw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Spinner opciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        opciones = (Spinner) findViewById(R.id.ddl_servicios);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.servicios_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        opciones.setAdapter(adapter);

        Button mConsultaServicio = (Button)findViewById(R.id.btn_consulta_servicio);

        mConsultaServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String servicio = "";
                String valor = "";

                switch (opciones.getSelectedItem().toString()){
                    case "Esterilización":
                        servicio = "esterilizacion";
                        break;
                    case "Control":
                        servicio = "control";
                        break;
                    default:
                        break;
                }

                TextView lbl_precio = (TextView)findViewById(R.id.textView);

                try {
                    valor = new ClienteWS(view.getContext()).execute(servicio).get();
                }catch (Exception e){
                    valor = "Sin Valor";
                }

                lbl_precio.setText("El precio por "+opciones.getSelectedItem().toString()+" es: $"+valor);

            }
        });

    }
}
