package com.marcelopintocruces.demonetworktisw;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    private RequestQueue queue;
    private StringRequest stringRequest;
    private TextView mResponse;
    private LocationManager lm;
    private LocationListener locationListener;
    private double latitude;
    private double longitude;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView mTextView = (TextView) findViewById(R.id.text);


        // Instanciamos la RequestQueue
        queue = Volley.newRequestQueue(this);
        //Seteamos la url a donde dirigiremos la petición
        String url ="http://tisw.marcelopintocruces.com";


        // Generaremos una petición de String, es decir, nos retornará el hipertexto tal cual como viene.
        // Se puede además realizar una petición de un objeto JSON, lo cual sería mas útil en caso de
        // interactual con una API Rest

        stringRequest = new StringRequest(Request.Method.GET, url, //Petición GET, a la url http://tisw.marcelopintocruces.com
                new Response.Listener<String>() { //Un hilo que espera el proceso de enviar la petición y esperar la respuesta
                    @Override
                    public void onResponse(String response) { //Este callback se invoca una vez que el servidor responde
                        mResponse = (TextView) findViewById(R.id.lbl_response);
                        mResponse.setText(response); //seteamos la respuesta del servidor en el textview
                        Log.i(TAG, "La respuesta del server es: "+ response);
                    }
                }, new Response.ErrorListener() { // además, al string request se le añade un parámetro de un listener de errores de red
            @Override
            public void onErrorResponse(VolleyError error) { //si hay un error en la petición, se puede manejar, tal como una exception
                Log.e("MainActivity", "Error al enviar petición");
            }
        });

        //RECORDAR: stringRequest debe ser añadido a una queue, de forma que android maneje su prioridad
        Button mSendRequest = (Button) findViewById(R.id.btn_aceptar);

        mSendRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                queue.add(stringRequest); //cuando el usuario aprieta el botón, se añade la petición a la queue.
            }
        });

        Button mVerCoordenadas = (Button) findViewById(R.id.btn_ver_coordenadas);

        /**
         * Ahora, la obtención de las coordenadas del GPS
         */

        mVerCoordenadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

                /**
                 * void requestLocationUpdates (String provider,: STRING CON EL NOMBRE DEL PROVEEDOR DE LA UBICACIÓN, TAL COMO GPS, INTERNET, O LA ÚLTIMA DEL USUARIO
                 long minTime,  : EL TIEMPO MÍMINO ENTRE INTERVALOS DE ACTUALIZACION, MEDIDO EN MILISEGUNDOS
                 float minDistance, : DISTANCIA MÍMINA ENTRE INTERVALOS DE ACTUALIZACION, MEDIDO EN METROS
                 LocationListener listener) : LISTENER QUE MANEJARÁ LAS ACTUALIZACIONES
                 */

                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        });

        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {

                //Este callback se invoca cada vez que hay una actualización de las coordenadas.
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                TextView mVerMisCoordenadas = (TextView) findViewById(R.id.lbl_coordenadas);
                mVerMisCoordenadas.setText("Latitud: "+location.getLatitude()+" Longitud: "+location.getLongitude());
                Log.i(TAG, location.toString());

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}

        };

        /**
         * Ahora, nueva activity, pasar lat y lon a la nueva activity y lanzar un intent
         *
         */

        Button mVerMiUbicacion = (Button) findViewById(R.id.btn_ver_ubicacion);


        mVerMiUbicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (latitude !=0 && longitude !=0){
                    //tenemos ubicación

                    //Creamos un nuevo Intent. Un intent significa INTENTO, es decir, intento de inicio de una nueva actividad de usuario
                    Intent i = new Intent(v.getContext(),UbicacionActivity.class);
                    i.putExtra("latitude", latitude);
                    i.putExtra("longitude", longitude);
                    Log.i(TAG, "Iniciando nueva actividad con lat:"+latitude+" y lon:"+longitude);
                    startActivity(i);

                }else{
                    Toast.makeText(MainActivity.this, "Aún no se ha obtenido la ubicación :(", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


}
